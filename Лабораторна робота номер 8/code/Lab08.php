<?php

/* Constant for autofill function */
define('MAX_VALUE', 3);

/* script time function start */
function script_time() {
    static $start = null;
    if (empty($start)) {
	$start = microtime(true);
    } else {
	$end = microtime(true);
        $execution_time = $end - $start;
	$start = null;
	return $execution_time;
    }
}

/* bus stations fill */
function fill_bus_stations() {
    for($times = 0; $times < MAX_VALUE; $times++) {
        $value = readline("Please, type the name of bus station: \n");
        //$value = rand();
        $bus_stations[] = $value;
    }
    return $bus_stations;
}

/* bus ways fill */
function fill_bus_ways(&$bus_ways) {
    for($times = 0; $times < MAX_VALUE; $times++) {
        $value = readline("Please, type the name for bus way: \n");
        //$value = rand();
        $bus_ways[$value] = fill_bus_stations();
    }
}

/* unset the element of bus_stations */
function unset_bus_station(&$bus_stations, $element) {
    if(!empty($bus_stations[$element])) {
        $bus_stations = array_diff($bus_stations, array($bus_stations[$element]));
        return true;
    } else {
        echo "This element do not exists!\n";
        return false;
    }
}

/* unset the element of bus_ways */
function unset_bus_way(&$bus_ways, $element_key) {
    if(!empty($bus_ways[$element_key])) {
        unset($bus_ways[$element_key]);
        return true;
    } else {
        echo "This element is empty!\n";
        return false;
    }
}

/*printing elements*/
function print_array_of_arrays($array) {
    foreach($array as $key => $value) {
        echo "[";
        foreach ($value as $index => $element) {
            if(!empty($value[$index + 1])) {
                echo "{$element}, ";
            } else {
                echo "{$element}";
            }
        }
        if($key != array_key_last($array)) {
            echo "],\n";
        } else {
            echo "]\n";
        }
    }
}

/* Init of variables */
$bus_ways = [];

/* script start */
script_time();
fill_bus_ways($bus_ways);
unset_bus_station($bus_ways[array_key_first($bus_ways)], 0);
unset_bus_way($bus_ways, array_key_last($bus_ways));
print_array_of_arrays($bus_ways);
unset($bus_ways);
$end = script_time();
echo "Script time equals: " . $end . " microseconds.\n";
