#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctime>
#include <iostream>

#define ARRAY_SIZE 2

using namespace std;

void BYTE(unsigned char A)
{
	for (int bit = 128; bit >= 1; bit >>= 1)
		cout << (A & bit ? '1' : '0');
	cout << ' ';
}

struct Board {
	string type;
	bool loaded;
	bool foreign;
};

void bit_struct_print(Board object)
{
	std::cout
		<< "Вивід на екран внутрішнє подання структури з варіантною частиною: "
		<< '\n';
	unsigned char *p = (unsigned char *)&object;
	for (int byte = 0; byte < sizeof(Board); byte++, p++) {
		if (byte && !(byte % 8)) {
			cout << endl;
		}
		BYTE(*p);
	}

	cout << endl;
	cout << endl;

	std::cout
		<< "Вивід на екран внутрішнє подання структури бітовими полями: "
		<< '\n';
	p = (unsigned char *)&object;
	for (int byte = 0; byte < sizeof(Board); byte++, p++) {
		BYTE(*p);
	}

	cout << endl;
	cout << endl;
}

int main()
{
	Board obj1 = { "Суховантажний", true, true };
	Board objects_array[] = { { "Суховантажний", false, false },
				  { "Танкер", true, false } };

	/* Вивід структури данних */
	std::cout << "Одна структура: " << '\n';
	unsigned int start_time_of_single_count = clock();
	bit_struct_print(obj1);
	unsigned int end_time_of_single_count = clock();
	unsigned int search_time_of_single_count =
		end_time_of_single_count - start_time_of_single_count;

	/* Вивід масиву структури данних */
	std::cout << "Масив структур: " << '\n';
	unsigned int start_time_of_array_count = clock();
	for (int i = 0; i < ARRAY_SIZE; i++) {
		bit_struct_print(objects_array[i]);
	}
	unsigned int end_time_of_array_count = clock();
	unsigned int search_time_time_of_array_count =
		end_time_of_array_count - start_time_of_array_count;

	/* Вивід часу та прирівняння часу */
	std::cout << "Час виконування алгоритму з однією структурою: "
		  << search_time_of_single_count << " мілісекунд\n";
	std::cout << "Час виконування алгоритму з масивом структур: "
		  << search_time_time_of_array_count << " мілісекунд\n";

	if (search_time_time_of_array_count > search_time_of_single_count) {
		std::cout << "Час виконування алгоритму з масивом більше."
			  << '\n';
	} else if (search_time_time_of_array_count <
		   search_time_of_single_count) {
		std::cout << "Час виконування алгоритму з масивом менше."
			  << '\n';
	} else {
		std::cout << "Час виконування обох алгоритмів однаковий."
			  << '\n';
	}

	return 0;
}
