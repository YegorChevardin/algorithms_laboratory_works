<?php

function array_print($array) {
    echo "[";
    foreach ($array as $key => $value) {
        if($key == array_key_last($array)) {
            echo $value;
        } else {
            echo $value . ", ";
        }
    }
    echo "]" . PHP_EOL;
}

function script_time() {
    static $start = null;
    if (empty($start)) {
	$start = microtime(true);
    } else {
	$end = microtime(true);
        $execution_time = $end - $start;
	$start = null;
	return $execution_time;
    }
}

function array_seed($elements) {
    $array = array();
    for($i = 0; $i < $elements; $i++) {
        $array[] = rand(0, 1000);
    }
    return $array;
}

function bubble_sort(&$array, $type = 'DESC') {
    $array_size = sizeof($array);
    $counter = 0;
    $linker = 0;
    
    if ($type == "ASC") {
        for($i = 0; $i < $array_size; $i++) {
           for ($j = $array_size - 1; $j > $i; $j--) {
               if ($array[$j - 1] < $array[$j]) {
                   $x = $array[$j - 1];
                   $array[$j - 1] = $array[$j];
                   $array[$j] = $x;
               }
               $counter++;
           }
           $linker++;
        }
        return [$counter, $linker];
    } else if ($type == "DESC") {
        for($i = 0; $i < sizeof($array); $i++) {
           for ($j = $array_size - 1; $j > $i; $j--) {
               if ($array[$j - 1] > $array[$j]) {
                   $x = $array[$j - 1];
                   $array[$j - 1] = $array[$j];
                   $array[$j] = $x;
               }
               $counter++;
           }
           $linker++;
        }
        return [$counter, $linker];
    } else {
        return 0;
    }
}

/* Init of variables */
$elements_to_seed = 50000;
$dynamic_array = array_seed($elements_to_seed);

array_print($dynamic_array);
script_time();
$result = bubble_sort($dynamic_array, 'ASC');
$count = $result[0];
$linker = $result[1];
$time = script_time();
array_print($dynamic_array);
echo "Time of sorting: " . $time . PHP_EOL;
echo "Count times: " . $count . PHP_EOL;
echo "Linker times: " . $linker . PHP_EOL;
