<?php
require_once 'LineList.php';

class ScriptOperation {
    private int $amount_of_search;
    private LineList $line_list;

    public function __construct($items_of_line_list, $content) {
        $this->line_list = new LineList($items_of_line_list, $content);
    }

    public function getAmountOfSearch() : int
    {
        return $this->amount_of_search;
    }

    public function scriptTime() {
        static $start = null;
        if (empty($start)) {
            $start = microtime(true);
        } else {
            $end = microtime(true);
            $execution_time = $end - $start;
            $start = null;
            return $execution_time;
        }
    }

    public function LinearBarrierSearch($key) : int {
        $this->amount_of_search = 0;

        $i = 0;
        $this->line_list->setElement($this->line_list->getItems(), $key);

        while ($this->line_list->getElement($i) != $key) {

            $this->amount_of_search++;
            $i++;
        }

        if($i == $this->line_list->getItems()) {
            return false;
        } else {
            return $i;
        }
    }

    public function BinarySearch($key) : int
    {
        $this->amount_of_search = 0;

        $current_position = 0;
        $amount_of_elements = $this->line_list->getItems();

        while ($current_position <= $amount_of_elements) {
            $this->amount_of_search++;

            $median = ($current_position + $amount_of_elements) / 2;

            if($this->line_list->getElement($median) >= $key) {
                $amount_of_elements = --$median;
            } else {
                $current_position = ++$median;
            }
        }

        if ($this->line_list->getElement($current_position) == $key) {
            return $current_position;
        } else {
            return false;
        }
    }
}