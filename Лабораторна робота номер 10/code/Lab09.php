<?php
require_once 'ScriptOperation.php';
require_once 'CsvReader.php';

/* Variables to change */
$key_to_find = 0;
$elements_in_array = 50000;
$file_name = "data.csv";

/* Print information to user */
echo "App options:\n";
echo "- Digit to find: " . $key_to_find . PHP_EOL;
echo "- Elements in array: " . $elements_in_array . PHP_EOL;
echo "- File name is: " . $file_name . "\n";

/* File object init */
$list_file = new CsvReader($file_name);
$list_file->fillFile($elements_in_array);
$list_file->readFile();

/* Linear barrier search section start */
$first_script = new ScriptOperation($elements_in_array, $list_file->getFileContent());

$first_script->scriptTime();

$result = $first_script->LinearBarrierSearch($key_to_find);
$time_result = $first_script->scriptTime();
$search_amount = $first_script->getAmountOfSearch();

echo "The result of linear barrier search is: " . $result  . ", amount of search amount: ". $search_amount . ", time result is: " . $time_result . PHP_EOL;

unset($first_script);
/* Linear barrier search section end */

/* Binary script section start */
$second_script = new ScriptOperation($elements_in_array, $list_file->getFileContent());

$second_script->scriptTime();
$result = $second_script->BinarySearch($key_to_find);
$time_result = $second_script->scriptTime();
$search_amount = $second_script->getAmountOfSearch();

echo "The result of binary search is: " . $result  . ", amount of search amount: ". $search_amount . ", time result is: " . $time_result . PHP_EOL;

unset($second_script);
/* Binary script section end */
