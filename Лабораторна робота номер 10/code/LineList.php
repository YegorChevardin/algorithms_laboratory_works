<?php

class LineList {
    private array $list;
    private int $items;

    public function __construct($items, $content) {
        $this->items = $items;
        $this->list = $content;
    }

    public function getList() : array
    {
        return $this->list;
    }
    public function setList($list) : void {
        $this->list = $list;
    }

    public function getItems() : int {
        return $this->items;
    }

    public function getElement($key) {
        if(!empty($this->list[$key])) {
            return $this->list[$key];
        } else {
            return $this->list[array_key_last($this->list)];
        }
    }

    public function setElement($key = 0, $value) : void {
        $this->list[$key] = $value;
    }

    public function printContent() : void {
        if(!empty($this->file_content[0])) {
            echo "Card number Name Age Street\n";
            foreach ($this->list as $value) {
                foreach ($value as $item) {
                    echo $value . " ";
                }
                echo "\n";
            }
        } else {
            echo "No content available!\n";
        }

    }
}