<?php

class CsvReader {
    private string $file_name;
    private array $file_content;

    public function __construct($file_name)
    {
        $this->file_name = $file_name;
    }

    public function getFileName() : string
    {
        return $this->file_name;
    }
    public function setFileName($file_name) : void
    {
        $this->file_name = $file_name;
    }

    public function getFileContent() : array {
        return $this->file_content;
    }

    public function printContent() : void {
        if(!empty($this->file_content[0])) {
            echo "Card number Name Age Street\n";
            foreach ($this->content as $value) {
                foreach ($value as $item) {
                    echo $value . " ";
                }
                echo "\n";
            }
        } else {
            echo "No content avaliable!\n";
        }

    }

    public function readFile() : void
    {
        $row = 1;

        if (($handle = fopen($this->file_name, "r")) !== false) {
            while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                $num = count($data);
                $row++;
                $this->file_content[] = $data;
            }
            fclose($handle);
        }
    }

    public function fillFile($elements_seeding_number) : void {
        $stream = fopen($this->file_name, 'w+0');
        for($i = 0; $i < $elements_seeding_number; $i++) {
            $card_number = rand(100000, 999999);
            $second_name = "Chevardin";
            $age = rand(18, 50);
            $adress = "Nikopol";

            $array = [$card_number, $second_name, $age, $adress];
            fputcsv($stream, $array);
        }
        fclose($stream);
    }
}
