<?php

function array_print($array) {
    echo "[";
    foreach ($array as $key => $value) {
        if($key == array_key_last($array)) {
            echo $value;
        } else {
            echo $value . ", ";
        }
    }
    echo "]" . PHP_EOL;
}

function script_time() {
    static $start = null;
    if (empty($start)) {
	$start = microtime(true);
    } else {
	$end = microtime(true);
        $execution_time = $end - $start;
	$start = null;
	return $execution_time;
    }
}

function array_seed($elements) {
    $array = array();
    for($i = 0; $i < $elements; $i++) {
        $array[] = rand(0, 1000);
    }
    return $array;
}

/* Sorting functions section start */
function hoara_sort($arr, $way = "ASC", &$counter = null, &$links = null)
{
    if (count($arr) <= 1){
        return $arr;
    }
 
    $pivot = $arr[0];
    $smaller = [];
    $equal = [];
    $greater = [];
    
    foreach ($arr as $x) {
        if($x > $pivot) {
            $links++;
            array_push($greater, $x);
        } else if($x < $pivot) {
            $links++;
            array_push($smaller, $x);
        } else {
            $links++;
            array_push($equal, $x);
        }
    }
    
    if ($way === "ASC") {
        $counter++;
        return array_merge(hoara_sort($smaller, $way, $counter, $links), $equal, hoara_sort($greater, $way, $counter, $links));
    } else if ($way === "DESC") {
        $counter++;
        return array_merge(hoara_sort($greater, $way, $counter, $links), $equal, hoara_sort($smaller, $way, $counter, $links));
    } else {
        echo "Error, way of sorting don't correct!" . PHP_EOL;
        return false;
    }
}

function bucket_sort($data, $way = "ASC", &$counter = null, &$links = null)
{
	$minValue = $data[0];
	$maxValue = $data[0];
	$dataLength = count($data);

	for ($i = 1; $i < $dataLength; $i++)
	{
		if ($data[$i] > $maxValue)
			$maxValue = $data[$i];
		if ($data[$i] < $minValue)
			$minValue = $data[$i];
	}

	$bucket = array();
	$bucketLength = $maxValue - $minValue + 1;
	
	for ($i = 0; $i < $bucketLength; $i++)
	{
		$bucket[$i] = array();
	}

	for ($i = 0; $i < $dataLength; $i++)
	{
		array_push($bucket[$data[$i] - $minValue], $data[$i]);
	}
        
        if($way === "ASC") {
            $k = 0;
            for ($i = 0; $i < $bucketLength; $i++)
            {
                    $counter++;
                    $bucketCount = count($bucket[$i]);

                    if ($bucketCount > 0)
                    {
                            for ($j = 0; $j < $bucketCount; $j++)
                            {
                                    $links++;
                                    $data[$k] = $bucket[$i][$j];
                                    $k++;
                            }
                    }
            }
            
            return $data;
        } else if($way === "DESC") {
            $k = 0;
            for ($i = $bucketLength - 1; $i >= 0; $i--)
            {
                    $counter++;
                    $bucketCount = count($bucket[$i]);

                    if ($bucketCount > 0)
                    {
                            for ($j = $bucketCount - 1; $j >= 0; $j--)
                            {
                                    $links++;
                                    $data[$k] = $bucket[$i][$j];
                                    $k++;
                            }
                    }
            }
            
            return $data;
        } else {
            echo "Error, way of sorting don't correct!" . PHP_EOL;
            return false;
        }
}

function merge_sort($my_array, $way = "ASC", &$counter, &$links){
    $counter++;
	if(count($my_array) == 1 ) return $my_array;
	$mid = count($my_array) / 2;
    $left = array_slice($my_array, 0, $mid);
    $right = array_slice($my_array, $mid);
	$left = merge_sort($left, $way, $counter, $links);
	$right = merge_sort($right, $way, $counter, $links);
	return merge($left, $right, $links);
}
function merge($left, $right, &$links){
	$res = array();
	while (count($left) > 0 && count($right) > 0){
		if($left[0] > $right[0]){
			$res[] = $right[0];
			$right = array_slice($right , 1);
		}else{
			$res[] = $left[0];
			$left = array_slice($left, 1);
		}
	}
	while (count($left) > 0){
            $links++;
		$res[] = $left[0];
		$left = array_slice($left, 1);
	}
	while (count($right) > 0){
            $links++;
		$res[] = $right[0];
		$right = array_slice($right, 1);
	}
	return $res;
}
/* Sorting functions section end */

/* Init of variables */
$elements_to_seed = 50000;
$array = array_seed($elements_to_seed);
$counter = null;
$links = null;

array_print($array);
script_time();
$sorted_array = merge_sort($array, "ASC", $counter, $links);
$time = script_time();
array_print($sorted_array);
echo "Times of equaling: " . $counter . PHP_EOL;
echo "Time of linking: " . $links . PHP_EOL;
echo "Time of sorting: " . $time . PHP_EOL;
