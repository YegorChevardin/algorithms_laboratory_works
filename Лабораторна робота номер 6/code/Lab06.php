<?php

/* Створення константи для двжини строки */
define("STRING_START_LENGTH", 0);
define ("STRING_END_LENGTH", 14);

/* Створення функції, що відповідає */
function script_time() {
    static $start = null;
    if (empty($start)) {
	$start = microtime(true);
    } else {
	$end = microtime(true);
        $execution_time = $end - $start;
	$start = null;
	return $execution_time;
    }
}

/* Створення функції для вимірювання памьяті */
function script_memory() {
   static $current_memory = null;
   if(empty($current_memory)) {
       $current_memory = memory_get_usage();
   } else {
       /* Used memory */
       $used_memory = memory_get_usage() - $current_memory;
       $current_memory = null;
       return $used_memory;
   }
}

/* Створення строки (Вектор постійної довжини) */
$vector_string = (string) substr(" Hello World! ", STRING_START_LENGTH, STRING_END_LENGTH);


/* Створення строки (Блочно-зв’язне подання із змінною довжиною) */
$block_string = [
    " Hello ",
    "World! "
];

/* Створення заданої функції */

function remowe_spaces_from_string($string, $block_f_strings_or_not = false) {
    if($block_f_strings_or_not == false) {
        $string = str_replace(' ', '', $string);
    } else if($block_f_strings_or_not == true) {
        foreach ($string as $key => $value) {
            $string[$key] = str_replace(' ', '', $value);
        }   
    } else {
        echo "An error was accured!\n";
        exit();
    }
    
    return $string;
}

/* Видалення пробілов з векторної строки */
script_memory();
script_time();
$result_vector_string = remowe_spaces_from_string($vector_string);
$vector_string_script_time = script_time();
$vector_memory_usage = script_memory();

/* Видалення пробілов з векторної строки */
script_memory();
script_time();
$result_block_string = remowe_spaces_from_string($block_string, true);
$block_string_script_time = script_time();
$block_memory_usage = script_memory();

/* Виведення результатів на єкран */
echo "Час виконання функції для векторної строки становить: " . $vector_string_script_time . " мікросекунд.\n";
echo "Час виконання функції для блочної строки становить: " . $block_string_script_time . " мікросекунд.\n";

echo "Використано поамьяті в векторному скрипту: " . $vector_memory_usage . " байт.\n";
echo "Використано поамьяті в блочному скрипту: " . $block_memory_usage . " байт.\n";

if($vector_string_script_time > $block_string_script_time) {
    $result_time = $vector_string_script_time - $block_string_script_time;
    echo "Час виконування векторного скрипта більше на {$result_time} мікросекунд.\n";
} else if($vector_string_script_time < $block_string_script_time) {
    $result_time = $block_string_script_time - $vector_string_script_time;
    echo "Час виконування блочного скрипта більше на {$result_time} мікросекунд.\n";
} else {
    echo "Час однаковий, різниці немає.\n";
}

if($vector_memory_usage > $block_memory_usage) {
    $result_memory_usage = $vector_memory_usage - $block_memory_usage;
    echo "Скрипт вектроний використовує більше памьяті на " . $result_memory_usage . " байт.\n";
} else if($vector_memory_usage < $block_memory_usage) {
    $result_memory_usage = $block_memory_usage - $vector_memory_usage;
    echo "Скрипт блочний використовує більше памьяті на " . $result_memory_usage . " байт.\n";
} else {
    echo "Обої скрипта викорситовують однакову памьять.\n";
}