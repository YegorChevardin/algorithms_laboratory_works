<?php
    $start = microtime(true);

    function palindrome($string) {
        if (strlen($string) <= 1) {
            return true;
        } else {
            if (substr($string, 0, 1) == substr($string, (strlen($string) - 1), 1)) {
                 return palindrome(substr($string, 1, strlen($string) - 2));
            } else {
                return false; 
            }
        }
    }
    
    $string = "radar";
    if(palindrome($string)) {
        echo $string . " is a palindrome.\n";
    } else {
        echo $string . " is not a palindrome.\n";
    }
    
    $end = (microtime(true) - $start);
    echo "Time of running recursive method is {$end}\n";
?>
