<?php
    $start = microtime(true);
    
    function palindrome($string) {
        $string = strtolower($string);
        $start = 0;
        $end = strlen($string) - 1;
        $break_point = 0;

        while($end > $start){
          if ($string[$start] != $string[$end]){
            $break_point = 1;
            break;
          }
          $start++;
          $end--;
        }

        if ($break_point == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    $string = "radar";
    if(palindrome($string)) {
        echo $string . " is a palindrome.\n";
    } else {
        echo $string . " is not a palindrome.\n";        
    }
    
    $end = (microtime(true) - $start);
    echo "Time of running iterative method is {$end}\n";
?>