<?php
/* Number of cals and rows constant */
define("NUMBER_OF_COLS_AND_ROWS", 5);

/* Get script time function */
function script_time() {
    static $start = null;
    if (empty($start)) {
	$start = microtime(true);
    } else {
	$end = microtime(true);
        $execution_time = $end - $start;
	$start = null;
	return $execution_time;
    }
}

/* Matrix to simple array converstion function */
function matrix_to_clear_array($matrix) {
    $array = [];
    foreach ($matrix as $key => $value) {
        foreach ($value as $index => $number) {
            if($number != 0) {
                $array[] = $number;
            }
        }
    }
    
    return $array;
}

/* Matrix or array search element */
function element_get($array, $row, $col, $matrix_or_not = false) {
    if($matrix_or_not == true) {
        return $array[$row - 1][$col - 1];
    } else {
        return $array[5*($row - 1) + ($col - 1)];
    }
}

/* Read matrix or array function */
function array_read($array, $matrix_or_not = false) {
    if($matrix_or_not == true) {
        echo "Вивід матриці: \n";
        foreach ($array as $value) {
            echo "[ ";
            foreach($value as $number) {
                echo "$number ";
            }
            echo "]\n";
        }
    } else {
        echo "Вивід зжатої матриці: \n";
        echo "[ ";
        foreach ($array as $value) {
            echo "$value ";
        }
        echo "]\n";
    }
}

/* Seed matrix function */
function seed_matrix() {
    $array = [];
    
    for ($i = 0; $i < NUMBER_OF_COLS_AND_ROWS; $i++) {
        for ($j = 0; $j < NUMBER_OF_COLS_AND_ROWS; $j++) {
            $array[$i][$j] = rand(1, 9);
        }
    }
    
    $array[1][0] = 0;
    $array[1][4] = 0;
    $array[2][0] = 0;
    $array[2][1] = 0;
    $array[2][4] = 0;
    $array[2][3] = 0;
    $array[3][0] = 0;
    $array[3][4] = 0;
    
    return $array;
}

/*Traditional way section start*/
echo "Старт традиційних алгоритмів\n";
script_time();

/* Інійіювання матриці з деякими елементами, що не потрібні (5 на 5) */
$matrix = seed_matrix();

/* Доступ до елементів во номеру стовпка та рядка */
$middle_element = element_get($matrix, 3, 3, true);
echo "Елемент матриці: matrix[2][2] = " . $middle_element . "\n";

/* Читання данних матриці */
array_read($matrix, true);

$traditional_script_time = script_time();
echo "Кінець традиційних алгоритмів, час виконання (мікросекунди): " . $traditional_script_time . "\n";
/* Traditional way section end */

/*New compressed section start*/
echo "Старт зжатих алгоритмів\n";
script_time();

/* Інійіювання матриці з деякими елементами, що не потрібні (5 на 5) */
$second_matrix = seed_matrix();

/* Зжаття матриці */
$second_matrix_compressed = matrix_to_clear_array($second_matrix);

/* Доступ до елементів во номеру стовпка та рядка */
$element = element_get($second_matrix, 3, 3, true);
echo "Елемент зжатої матриці: second_matrix[2][2] = " . $element . "\n";

/* Доступ до елементів во номеру стовпка та рядка зжатої матриці */
$element = element_get($second_matrix_compressed, 1, 9);
echo "Елемент зжатої матриці: second_matrix_compressed[0][9] = " . $element . "\n";

/* Читання данних матриці */
array_read($second_matrix, true);

/* Читання данних зжатої матриці */
array_read($second_matrix_compressed);

$compressed_script_time = script_time();
echo "Кінець традиційних алгоритмів, час виконання (мікросекунди): " . $compressed_script_time . "\n";
/* Traditional way section end */

/* Time results section start */
if($traditional_script_time > $compressed_script_time) {
    $result_time = $traditional_script_time - $compressed_script_time;
    echo "Час виконування традиційного скрипта більше на {$result_time} мікросекунд.\n";
} else if($compressed_script_time > $traditional_script_time) {
    $result_time = $compressed_script_time - $traditional_script_time;
    echo "Час виконування скрипта з зжатою матрицею більше на {$result_time} мікросекунд.\n";
} else {
    echo "Час однаковий, різниці немає.\n";
}