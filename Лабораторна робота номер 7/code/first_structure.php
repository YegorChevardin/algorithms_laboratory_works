<?php
define('QUEUE_MAX_LENGTH', 10);

/* script time function start */
function script_time() {
    static $start = null;
    if (empty($start)) {
	$start = microtime(true);
    } else {
	$end = microtime(true);
        $execution_time = $end - $start;
	$start = null;
	return $execution_time;
    }
}

/* Init of queue */
function queue_init(&$top, &$bottom) {
    $top = $bottom = 0;
}

/* Clear queue */
function queue_clear(&$top, &$bottom) {
    $top = $bottom;
}

/* Getting queue size */
function get_queue_size(&$top, &$bottom) {
    if($top <= $bottom) {
      return ($bottom - $top);  
    } else {
        return ($bottom + QUEUE_MAX_LENGTH - $top);
    }
}

/* Push new element */
function queue_push(&$queue, &$top, &$bottom, $item) {
  
  if(($bottom + 1) % QUEUE_MAX_LENGTH == $top) {
      echo "FIFO Full\n";
      return 0;   
   }
   $queue[$bottom] = $item;
   $bottom = ($bottom + 1) % QUEUE_MAX_LENGTH;
   return 1;
}

/* get first element */
function queue_get_top(&$queue, &$top, &$bottom) {
    if($top == $bottom) {
        echo "FIFO Empty\n";
        return 0;
    }
    $top_element = $queue[$top];
    $top = ($top + 1) % QUEUE_MAX_LENGTH;
    return $top_element;
}

/* Init of variables */
$top;
$bottom;
$queue = [];

script_time();
queue_init($top, $bottom);
echo get_queue_size($top, $bottom) . "\n";
echo queue_get_top($queue, $top, $bottom) . "\n";
queue_push($queue, $top, $bottom, 1);
queue_push($queue, $top, $bottom, 100);
echo queue_get_top($queue, $top, $bottom) . "\n";
$size = get_queue_size($top, $bottom);
echo $size . "\n";
var_dump($queue);
queue_clear($top, $bottom);
echo get_queue_size($top, $bottom) . "\n";
$end = script_time();
echo "Script time equals: " . $end . "microseconds.\n";
