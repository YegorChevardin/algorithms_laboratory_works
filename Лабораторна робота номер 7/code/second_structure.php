<?php
define('QUEUE_MAX_LENGTH', 10);

/* script time function start */
function script_time() {
    static $start = null;
    if (empty($start)) {
	$start = microtime(true);
    } else {
	$end = microtime(true);
        $execution_time = $end - $start;
	$start = null;
	return $execution_time;
    }
}

/* Init of queue */
function queue_init() {
    $queue = [];
    
    for ($i = 0; $i < (QUEUE_MAX_LENGTH / 2); $i++) {
        $queue[] = rand();
    }
    
    return $queue;
}

/* Clear_queue */
function queue_clear(&$queue) {
    if(!empty($queue[0])) {
        $queue = array_diff($queue, array($queue[0]));
        return true;
    } else {
        echo "The queue is empty!\n";
        return false;
    }
}

/* Unset the queue */
function unset_queue(&$queue) {
    unset($queue);
}

/* Getting queue size */
function queue_size(&$queue) {
    $size = sizeof($queue);
    echo "Queue size is {$size} elements.\n";
    
   foreach($queue as $value) {
       echo $value . " ";
   }
   echo "\n";
   
   return $size;
}

/* Get first element from queue */
function get_first(&$queue) {
    if(!empty($queue[0])) {
        return $queue[0];
    } else {
        echo "Queue is empty!\n";
        return false;
    }
}

/*Push element in_queue*/
function push_element(&$queue, $item) {
  array_push($queue, $item);
  if(count($queue) < QUEUE_MAX_LENGTH){
    echo "Element pushed successfully.\n";
    return true;
  } else {
      echo "The queue is full!\n";
      return array_slice($queue, (count($queue)- QUEUE_MAX_LENGTH), QUEUE_MAX_LENGTH);
  }
}

/* Init of variables */
$queue = queue_init(); // Initing the queue by seeding it 5 random elements

script_time();
queue_init();
queue_size($queue);
$current_first_element = get_first($queue);
echo "The first element now is: " . $current_first_element . "\n";
push_element($queue, 1);
push_element($queue, 100);
queue_clear($queue);
$current_first_element = get_first($queue);
echo "The first element now is: " . $current_first_element . "\n";
queue_size($queue);
unset_queue($queue);
$end = script_time();
echo "Script time equals: " . $end . "microseconds.\n";