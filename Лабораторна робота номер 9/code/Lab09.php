<?php
require_once 'ScriptOperation.php';

/* Variables to change */
$digit_to_find = 17;
$elements_in_array = 10000;

/* Print information to user */
echo "Digit to find: " . $digit_to_find . PHP_EOL;
echo "Elements in array: " . $elements_in_array . PHP_EOL;

/* Linear barrier search section start */
$first_script = new ScriptOperation($elements_in_array);

$first_script->scriptTime();
$result = $first_script->LinearBarrierSearch($digit_to_find);
$time_result = $first_script->scriptTime();
$search_amount = $first_script->getAmountOfSearch();

echo "The result of linear barrier search is: " . $result  . ", amount of search amount: ". $search_amount . ", time result is: " . $time_result . PHP_EOL;

unset($first_script);
/* Linear barrier search section end */

/* Binary script section start */
$second_script = new ScriptOperation($elements_in_array);

$second_script->scriptTime();
$result = $second_script->BinarySearch($digit_to_find);
$time_result = $second_script->scriptTime();
$search_amount = $second_script->getAmountOfSearch();

echo "The result of linear barrier search is: " . $result  . ", amount of search amount: ". $search_amount . ", time result is: " . $time_result . PHP_EOL;

unset($second_script);
/* Binary script section end */
