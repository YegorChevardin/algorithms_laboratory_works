<?php

class LineList {
    private int $items;
    private array $list;

    public function __construct($items) {
        $this->items = $items;

        $this->initList();
    }

    public function __destruct() {
        unset($this->list);
        unset($this->items);
    }

    public function getItems(): int
    {
        return $this->items;
    }

    public function getList(): array
    {
        return $this->list;
    }

    public function initList(): void
    {
        for($i = 0; $i < $this->items; $i++) {
            $this->list[$i] = $i;
        }
    }

    public function unsetElement($key): bool
    {
        if(!empty($this->list[$key])) {
            $this->list = array_diff($this->list, array($this->list[0]));
            return true;
        } else {
            return false;
        }
    }

    public function getElement($key) : int {
        if(!empty($this->list[$key])) {
            return $this->list[$key];
        } else {
            return false;
        }
    }
    public function setElement($key, $value) : void
    {
        if(!empty($this->list[$key])) {
            $this->list[$key] = $value;
        } else {
            if($key > $this->items) {
                $key = $this->items;
            }

            $this->list[$key] = $value;
            $this->items++;
        }
    }

    public function printAll(): void
    {
        echo "[";
        foreach($this->list as $key => $value) {
            if($key === array_key_last($this->list)) {
                echo $value;
            } else {
                echo $value . ", ";
            }
        }
        echo "]" . PHP_EOL;
    }
}