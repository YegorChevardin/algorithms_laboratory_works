#include "VectorContainer.h"

VectorContainer::VectorContainer()
{
}

VectorContainer::VectorContainer(int size)
{
	srand(time(NULL));

	for (int i = 0; i < size; i++) {
		this->array.emplace_back(rand() % 10);
	}
}

VectorContainer::VectorContainer(const VectorContainer &copy)
{
	for (int i = 0; i < this->array.size(); i++) {
		this->array.push_back(copy.array[i]);
	}
}

VectorContainer::~VectorContainer() = default;

int VectorContainer::getSize() const
{
	return this->array.size();
}

void VectorContainer::addElement(const int element, size_t position)
{
	this->array.insert(this->array.begin() + position - 1, element);
}

void VectorContainer::removeElement(const size_t index)
{
	this->array.erase(this->array.begin() + index);
}

int VectorContainer::getElement(size_t index) const
{
	if (index > this->array.size()) {
		index = this->array.size() - 1;
	}

	return this->array[index];
}

void VectorContainer::setElement(size_t index, int number)
{
	if (index > this->array.size()) {
		index = this->array.size() - 1;
	}

	this->array[index] = number;
}

void VectorContainer::printArray() const
{
	cout << "[";
	for (int value : this->array) {
		cout << value << " ";
	}
	cout << "]" << endl;
}

int VectorContainer::getIndexOfElement(int value) const
{
	auto it = find(this->array.begin(), this->array.end(), value);

	if (it != this->array.end()) {
		int index = it - this->array.begin();
		return index;
	} else {
		cout << "No element found!" << endl;
		return -1;
	}
}

void VectorContainer::sortArray(const string way)
{
	if (way == "ASC") {
		for (int i = 0; i < this->array.size(); i++) {
			for (int j = this->array.size() - 1; j > i; j--) {
				if (this->array[j - 1] < this->array[j]) {
					int x = array[j - 1];
					this->array[j - 1] = this->array[j];
					this->array[j] = x;
				}
			}
		}
	} else if (way == "DESC") {
		for (int i = 0; i < this->array.size(); i++) {
			for (int j = this->array.size() - 1; j > i; j--) {
				if (this->array[j - 1] > this->array[j]) {
					int x = this->array[j - 1];
					this->array[j - 1] = this->array[j];
					this->array[j] = x;
				}
			}
		}
	} else {
		cout << "Please, type correct way of sorting!" << endl;
	}
}
