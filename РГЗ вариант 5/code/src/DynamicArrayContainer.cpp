#include "DynamicArrayContainer.h"

DynamicArrayContainer::DynamicArrayContainer() : size(1)
{
}

DynamicArrayContainer::DynamicArrayContainer(int size)
{
	srand(time(NULL));

	this->array = (int *)malloc(size * sizeof(int));
	this->size = size;

	for (int i = 0; i < this->size; i++) {
		this->array[i] = rand() % 10;
	}
}

DynamicArrayContainer::DynamicArrayContainer(const DynamicArrayContainer &copy)
	: size(copy.size)
{
	free(this->array);
	this->array = copy.array;
}

DynamicArrayContainer::~DynamicArrayContainer()
{
	free(DynamicArrayContainer::array);
};

int &DynamicArrayContainer::getArray() const
{
	return *this->array;
}

int DynamicArrayContainer::getSize() const
{
	return this->size;
}

void DynamicArrayContainer::addElement(const int element, size_t position)
{
	int *new_array = (int *)malloc((this->size + 1) * sizeof(int));

start_checking_position:
	if (position > DynamicArrayContainer::size) {
		position = DynamicArrayContainer::size + 1;
		for (int i = 0; i < DynamicArrayContainer::size; i++) {
			new_array[i] = DynamicArrayContainer::array[i];
		}
		new_array[position - 1] = element;
	} else if (position == 0) {
		position = 1;
		goto start_checking_position;
	} else {
		for (int i = 0; i < position - 1; i++) {
			new_array[i] = DynamicArrayContainer::array[i];
		}

		new_array[position - 1] = element;

		for (int i = position; i < DynamicArrayContainer::size + 1;
		     i++) {
			new_array[i] = DynamicArrayContainer::array[i - 1];
		}
	}

	free(DynamicArrayContainer::array);
	DynamicArrayContainer::array = new_array;
	DynamicArrayContainer::size++;
}

void DynamicArrayContainer::removeElement(size_t index)
{
	int *new_array = (int *)malloc((this->size - 1) * sizeof(int));

	if (index > DynamicArrayContainer::size - 1) {
		index = DynamicArrayContainer::size - 1;

		for (int i = 0; i < index; i++) {
			new_array[i] = DynamicArrayContainer::array[i];
		}
	} else {
		for (int i = 0; i < index; i++) {
			new_array[i] = DynamicArrayContainer::array[i];
		}

		for (int i = DynamicArrayContainer::size - 1; i > index; i--) {
			new_array[i - 1] = DynamicArrayContainer::array[i];
		}
	}
	free(DynamicArrayContainer::array);
	DynamicArrayContainer::array = new_array;
	DynamicArrayContainer::size--;
}

int DynamicArrayContainer::getElement(size_t index) const
{
	if (index > this->size - 1) {
		index = this->size - 1;
	}

	return this->array[index];
}

void DynamicArrayContainer::setElement(size_t index, int number)
{
	if (index > this->size - 1) {
		index = this->size - 1;
	}

	this->array[index] = number;
}

void DynamicArrayContainer::printArray() const
{
	cout << "[";
	for (int i = 0; i < this->size; i++) {
		cout << this->array[i] << " ";
	}
	cout << "]" << endl;
}

void DynamicArrayContainer::sortArray(const string &way)
{
	if (way == "ASC") {
		for (int i = 0; i < this->size; i++) {
			for (int j = this->size - 1; j > i; j--) {
				if (this->array[j - 1] < this->array[j]) {
					int x = array[j - 1];
					this->array[j - 1] = this->array[j];
					this->array[j] = x;
				}
			}
		}
	} else if (way == "DESC") {
		for (int i = 0; i < this->size; i++) {
			for (int j = this->size - 1; j > i; j--) {
				if (this->array[j - 1] > this->array[j]) {
					int x = this->array[j - 1];
					this->array[j - 1] = this->array[j];
					this->array[j] = x;
				}
			}
		}
	} else {
		cout << "Please, type correct way of sorting!" << endl;
	}
}

int DynamicArrayContainer::getIndexOfElement(int value) const
{
	int index_to_find;

	for (int i = 0; i < this->size; i++) {
		if (this->array[i] == value) {
			index_to_find = i;
		}
	}

	return index_to_find;
}

int DynamicArrayContainer::getMinElement() const
{
	int min_element = this->array[0];

	for (int i = 0; i < this->size; i++) {
		if (min_element > this->array[i]) {
			min_element = this->array[i];
		}
	}

	return min_element;
}
