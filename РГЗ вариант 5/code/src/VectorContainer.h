#pragma once
#ifndef VectorContainer_H
#define VectorContainer_H

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <vector>
#include <bits/stdc++.h>

using std::cout;
using std::endl;
using std::find;
using std::string;
using std::vector;

class VectorContainer {
    private:
	vector<int> array;

    public:
	VectorContainer();

	VectorContainer(int size);

	VectorContainer(const VectorContainer &copy);

	~VectorContainer();

	int getSize() const;

	void addElement(const int element, size_t position = 1);

	void removeElement(size_t index = 0);

	int getElement(size_t index) const;

	void setElement(size_t index, int number);

	void printArray() const;

	void sortArray(const string way);

	int getIndexOfElement(int value) const;
};
#endif //VectorContainer_H
