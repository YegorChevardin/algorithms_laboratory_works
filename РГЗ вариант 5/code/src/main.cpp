#include "DynamicArrayContainer.h"
#include "VectorContainer.h"
#include <chrono>

using namespace std::chrono;

int main()
{
	/* Init */
	DynamicArrayContainer array_1(50);
	VectorContainer vector_1(50);

	/* Printing two arrays */
	array_1.printArray();
	vector_1.printArray();

	auto start = high_resolution_clock::now();
	array_1.sortArray("DESC");
	array_1.getElement(4);
	auto stop = high_resolution_clock::now();
	auto duration = duration_cast<microseconds>(stop - start);
	cout << "Getting element form array in microseconds: "
	     << duration.count() << endl;

	auto vstart = high_resolution_clock::now();
	vector_1.sortArray("DESC");
	vector_1.getElement(4);
	auto vstop = high_resolution_clock::now();
	auto vduration = duration_cast<microseconds>(vstop - vstart);
	cout << "Getting element form vector in microseconds: "
	     << vduration.count() << endl;

	return 0;
}
