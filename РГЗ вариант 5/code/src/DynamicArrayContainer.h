#pragma once
#ifndef DYNAMICARRAYCONTAINER_H
#define DYNAMICARRAYCONTAINER_H

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <ctime>

using std::cout;
using std::endl;
using std::string;

class DynamicArrayContainer {
    private:
	int *array;
	int size;

    public:
	DynamicArrayContainer();

	DynamicArrayContainer(int size);

	DynamicArrayContainer(const DynamicArrayContainer &copy);

	~DynamicArrayContainer();

	int &getArray() const;

	int getSize() const;

	void addElement(const int element, size_t position = 1);

	void removeElement(size_t index);

	int getElement(size_t index) const;

	void setElement(size_t index, int number);

	void printArray() const;

	void sortArray(const string &way);

	int getIndexOfElement(int value) const;

	int getMinElement() const;
};
#endif //DYNAMICARRAYCONTAINER_H
