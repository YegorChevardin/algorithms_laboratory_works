<?php
echo "Welcome to convertion programm, choose, what type of data you will convert:\n1 - short integer;\n2 - long double;\n3 - char;\n4 - N length array of integers;\n5 - exit program.\n";
input:
$answer = (int) readline("You answer: ");

switch ($answer) {
    case 1:
        short_integer_converting_program();
        break;
    case 2:
        long_double_converting_program();
        break;
    case 3:
        char_converting_program();
        break;
    case 4:
        elem_arr_input:
        $elem_numbers = readline("How much elements your array will contain (minimum 1, maximum 10): ");
        if($elem_numbers < 1 or $elem_numbers > 10) {
            echo "Wrong numbers of array.\n";
            goto elem_arr_input;
        }
        array_int_converting_program($elem_numbers);
        break;
    case 5:
        exit();
    default:
        echo "You picked wrong number, try again.\n";
        goto input;
}


/* Functions section start */
function short_integer_converting_program() {
    short_int_input:
    $short_int = (int) readline("Please, type unsigned short integer number (from 0 to 65535): ");

    if($short_int != false and gettype($short_int) == "integer" and $short_int >= 0 and $short_int <= 65535) {
        $start = microtime(true);
        $result = decbin($short_int);
        $end = (microtime(true) - $start);
    } else {
        echo "You entered wrong number, try again.\n";
        goto short_int_input;
    }

    echo "The result of short int converting is " . $result . "\n";

    echo "Time of running recursive method is {$end}\n";

    return $result;
}

function floatToBinStr($value) {
    $bin = '';
    $packed = pack('d', $value); // use 'f' for 32 bit
    foreach(str_split(strrev($packed)) as $char) {
        $bin .= str_pad(decbin(ord($char)), 8, 0, STR_PAD_LEFT);
    }
    return $bin;
}

function long_double_converting_program() {
    input_first:
    $first_part = (int) readline("Please, type integer part of unsigned long double number: ");
    input_second:
    $second_part = (int) readline("Please, type float part of unsigned long double number: ");

    if($second_part <= 0) {
        echo "Your number is fully integer, type float part correctly\n";
        goto input_second;
    } else if ($second_part > 2147483647) {
        echo "Your float part is too long for long double number, you will run our of memory, please try another float part.\n";
        goto input_second;
    } else {
        $long_double = (double) "{$first_part}.{$second_part}";
    }

    if($long_double != false and gettype($long_double) == "double" and $long_double >= 0) {
        $start = microtime(true);
        $result = floatToBinStr($long_double);
        $end = (microtime(true) - $start);
    } else {
        echo "You entered wrong number, try again.\n";
        goto input_first;
    }

    echo "The result of long double converting is " . $result . "\n";

    echo "Time of running recursive method is {$end}\n";

    return $result;
}

function charToBin($input)
{
    if (!is_string($input))
        return false;
    $value = unpack('H*', $input);
    return base_convert($value[1], 16, 2);
}

function char_converting_program() {
    char_input:
    $one_char = (string) readline("Please, type one character (If you'll type string, the first char will be taken): ");

    if($one_char == false) {
        echo "Please, try to type again.\n";
        goto char_input;
    } else {
        if (isset($one_char[1])) {
            $one_char = $one_char[0];
            echo "The char will be: " . $one_char . "\n";
        }
    }

    if(gettype($one_char) == "string") {
        $start = microtime(true);
        $result = charToBin($one_char);
        $end = (microtime(true) - $start);
    } else {
        echo "You entered wrong type, try again.\n";
        goto char_input;
    }

    echo "The result of char converting is " . $result . "\n";

    echo "Time of running recursive method is {$end}\n";

    return $result;
}

function array_int_converting_program($numbers_of_elements) {
    $int_array = [];

    input:
    for($i = 0; $i < $numbers_of_elements; $i++) {
        $int_array[$i] = (int) readline("Please, type unsigned integer (bigger than 0) for " . ($i + 1) . " element in array: ");
        if(gettype($int_array[$i]) != "integer" or $int_array[$i] < 0) {
            echo "You typed wrong type, please, try to write array again.\n";
            goto input;
        }
    }

    echo "You have this array: ";
    var_dump($int_array);

    $start = microtime(true);
    foreach ($int_array as $key => $value) {
        $int_array[$key] = decbin($value);
    }
    $end = (microtime(true) - $start);

    echo "The result of array converting is:\n";
    var_dump($int_array);

    echo "Time of running recursive method is {$end}\n";

    return $int_array;
}
